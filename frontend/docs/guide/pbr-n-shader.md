![](https://s2.loli.net/2022/07/01/iwHd4xuBoUKWVPZ.png)

*****

# PBR效果与光影推荐

目前Minecraft主流的光影Mod有Optifine和Iris Shaders。但我们推荐使用Optifine，因为其集成了方块小镇Mod所需要的CTM效果，而目前Iris Shaders模组无法做到。

方块小镇Mod内部分方块具有PBR效果，例如能反光的积水路面，具有反射效果的钢材，窗户等。于此，我们推荐安装一个支持此效果的光影，如BSL光影。而BSL光影的PBR效果需手动打开，以BSL\_V8.0为例，您需在视频设置-光影-光影设置-Material中将Advanced Materials选项打开。

我们推荐使用非SEUS系的光影包，因为大部分SEUS系光影对半透明方块的支持不是非常友好。