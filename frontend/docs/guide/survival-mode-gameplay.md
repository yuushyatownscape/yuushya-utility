![](https://s2.loli.net/2022/07/01/aVJLnU12KcwrkXb.png)

*****

# 生存模式玩法

Yuushya以一种巧妙，适合生存玩家的方式提供了所有建材的生存获取途径。

您可以在游戏内通过简单的合成获取建材的【蓝图】。把蓝图放入切石机中即可挑选需要的建材。

<div style="display:flex; justify-content:center; padding:30px;"><img src="https://s2.loli.net/2022/07/01/9emBwxEfGj31MSg.png" style="zoom:40%;" /></div>

目前已有15个不同的【蓝图】，分别可以获取对应小类的建材。【蓝图】的合成表(无序合成)如下。

<div style="display:flex; justify-content:center; padding:30px;"><img src="https://s2.loli.net/2022/07/01/9md6UIYE3ZLlhiA.png" style="zoom:36%;" /></div>