
![](https://s2.loli.net/2022/07/01/KitkHDQWBXgVETN.png)

*****

# 一站式安装教程
 
Yuushya是结合Mod与资源包的建筑制作系列。

下载模组前请安装好资源包和对应的Mod API(Fabric或Forge)之后，除安装我们提供的Mod以及资源包外，您还需安装例如Optifine、Continuity等连接纹理模组来实现模组内置的方块效果。

所有您需要知道的有关安装的事宜都在此篇【一站式安装教程中】，竭诚为您服务！

*****

## 安装方法

① 通过下载页面下载【方块小镇轻量整合包】一键安装好所需模组、资源包以及演示存档。在此推荐使用 [PCL2启动器](https://www.mcbbs.net/thread-719579-1-1.html) 或 [HMCL启动器](https://hmcl.huangyuhui.net/) 加载下载的整合包文件。[这里是一个通过整合包格式安装的官方视频教程](https://www.bilibili.com/video/BV1VN411a7TU/)

② 通过查阅下方的安装表格，自行安装所需模组

*****

## 安装表格

不需要考虑太多！遵循以下步骤下载对应模组，可以保证您顺利玩上方块小镇。但首先你要考虑好需要的是您需要的API是Forge还是Fabric

#### Forge全版本通用表格

|  名称   | 属性 | 注释  |
|  ----  | ---- |----  |
| 方块小镇模组  | 核心 |  |
| 方块建模模组 | 推荐安装 | 推荐安装以获得完整模组体验 |
| Architectury API   | 必须安装 | 前置模组 |
| *连接纹理模组*   | 必须安装 | 提供模组所需的连接纹理功能，见下 |

#### Fabric全版本通用表格

|  名称   | 属性 | 注释  |
|  ----  | ---- |----  |
| 方块小镇模组  | 核心 |  |
| 方块建模模组 | 推荐安装 | 推荐安装以获得完整模组体验 |
| Architectury API   | 必须安装 | 前置模组 |
| Fabric API   | 必须安装 | 前置模组 |
| *连接纹理模组*   | 必须安装 | 提供模组所需的连接纹理功能，见下 |

*****

#### 连接纹理模组

|  名称   | 版本 | 注释  |
|  ----  | ---- |----  |
| Optifine  | 1.16.5 | **1.16.5 首选** |
| Optifine | 1.18.2  | 1.18.2 Forge & Fabric 备选 |
| ConnectedTextures Mod | 1.18.2  | **1.18.2 Forge 首选** |
| Continuity | 1.18.2  | **1.18.2 Fabric 首选** |
| Optifine | 1.19.2  | **1.19.2 Forge 首选** |
| Continuity | 1.19.2  | **1.19.2 Fabric 首选** |
| Optifine | 1.19.4  | **1.19.4 Forge 首选** |
| Continuity | 1.19.4  | **1.18.4 Fabric 首选** |
| Optifine | 1.20.1  | **1.20.1 Forge 首选** |
| Continuity | 1.20.1  | **1.20.1 Fabric 首选** |

*****

## 其它可选辅助模组

#### Patchouli 手册

Patchouli模组是提供手册功能的功能模组，您可选择安装Patchouli模组来浏览模组内置的玩法手册。

#### WorldEdit 创世神

WorldEdit模组提供了许多建筑玩家需要的实用功能，推荐下载和学习以提高您的建筑效率。

#### Iris 光影

Iris是目前Fabric下主流的光影模组。下载Iris之前，您需要安装其前置Sodium。因为Fabric下模组还需安装Continuity模组，为了使Continuity与Sodium兼容，您还需安装Indium模组

#### Replaymod 回放及延时摄影

Replaymod是当前主流的Minecraft延时动画录制导出模组，目前在高版本仅存在于Fabric平台下。如果您需要Replaymod与Iris模组共存，您需要在Replaymod的官网下载特制版本的Sodium模组（Iris前置）。前往https://www.replaymod.com/download/ ，在下载一栏寻找到 *(Click to show compatible Sodium versions)* 字样，下载对应的Sodium模组。


## 相关链接

[Optifine 模组](http://www.optifine.net/)

[Replaymod 回放及延时摄影模组](https://www.replaymod.com/)

[Patchouli 手册模组(MC百科)](https://www.mcmod.cn/class/1388.html)

[WorldEdit 创世神模组(MC百科)](https://www.mcmod.cn/class/609.html)

[Iris 光影模组(MC百科)](https://www.mcmod.cn/class/3697.html)