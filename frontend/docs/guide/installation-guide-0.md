
![](https://s2.loli.net/2022/07/01/KitkHDQWBXgVETN.png)

*****

# 一站式安装教程
 
Yuushya是结合Mod与资源包的建筑制作系列。

下载模组前请安装好资源包和对应的Mod API(Fabric或Forge)之后，除安装我们提供的Mod以及资源包外，您还需安装例如Optifine、Continuity等模组来实现模组内置的方块效果。

所有您需要知道的有关安装的事宜都在此篇【一站式安装教程中】，竭诚为您服务！

*****

## 视频教程

#### Fabric版安装教程
https://www.bilibili.com/video/BV1Vi4y1o7Ws

#### Forge版安装教程
https://www.bilibili.com/video/BV1bP4y1P7Fo

*****

## 懒人安装表格

不需要考虑太多！遵循以下步骤，至少可以保证您顺利玩上模组！但首先你要考虑好需要的是您需要的API是Forge还是Fabric

本次安装过程都采用PCL启动器安装，原理对于其他启动器一样。

#### Forge 1.18.2

|  下载   | 版本 | 注释  |
|  ----  | ---- |----  |
| Forge 最新版 + Optifine 1.18.2 H9 pre3  |  | API，请选择PCL里的自动安装 |
| 方块小镇模组 | Forge 1.18.2 最新版 | 核心模组 |
| Architectury API   | Forge 1.18.2 最新版 | 方块小镇模组 |

#### Fabric 1.18.2

|  下载   | 版本 | 注释  |
|  ----  | ---- |----  |
| Fabric 最新版  |  | API，请选择PCL里的自动安装 |
| 方块小镇模组 | Fabric 1.18.2 最新版 | 核心模组 |
| Fabric API | Fabric 1.18.2 最新版 | 前置模组 |
| Architectury API   | Fabric 1.18.2 最新版 | 方块小镇前置模组 |
| Continuity   | Fabric 1.18.2 最新版 | Optifine CTM 功能替代模组 |
| Indium   | Fabric 1.18.2 最新版 | Continuity 前置模组 |

*****

## 其它可选辅助模组

#### Patchouli 手册

Patchouli模组是提供手册功能的功能模组，您可选择安装Patchouli模组来浏览模组内置的玩法手册。

#### WorldEdit 创世神

WorldEdit模组提供了许多建筑玩家需要的实用功能，推荐下载和学习以提高您的建筑效率。

#### Iris 光影

Iris是目前Fabric下主流的光影模组。下载Iris之前，您需要安装其前置Sodium。

#### Replaymod 录制

Replaymod是当前主流的Minecraft延时动画录制导出模组，目前在高版本仅存在于Fabric平台下。如果您需要Replaymod与Iris模组共存，您需要在Replaymod的官网下载特制版本的Sodium模组（Iris前置）。前往https://www.replaymod.com/download/ ，在下载一栏寻找到 *(Click to show compatible Sodium versions)* 字样，下载对应的Sodium模组。


## 相关链接

[Optifine模组](http://www.optifine.net/)

