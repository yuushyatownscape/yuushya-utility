

*****

# 教程 - 旧版（BETA版）存档转换

## 关于存档转换

由于旧版模组物品ID与新版本不同，在新版模组下打开旧版存档是损坏的。

⚠ **因此注意，请在最新版模组（正式版）下加载旧存档前使用存档转换工具转换存档！**

⚠ **若不转换存档，您将可正常进入地图，但地图内所有的旧版模组建材都会消失！**

此存档转换并不十全十美，不保证您的极个别建材会集体消失或是转化为另一种建材。但是我们保证这不会对您的地图造成太大影响。


## 存档转换工具使用

**⚠ 在这之前，我们强烈建议将您的原存档备份一份。因为以下操作是直接对存档本身进行的，且不可撤销！**

下载[【存档转换工具】](https://yuushya.com/townscape/file/convert-tools.zip)，解压后双击运行其中的yuushya-tool.bat，在此之前请确保你的Java已安装

<div style="display:flex; justify-content:center; padding:30px;"><img src="https://s2.loli.net/2023/04/22/BfH8gEG4FQ5CAad.png" style="zoom: 50%;" /></div>

打开应用程序后，点击【存档转换器】按钮。打开之前会提示“正在加载依赖，如果没有新窗口出现，请重新打开软件”。若重启软件后仍未出现新窗口，请前往 [存档转换常见问题](/save-convert-qna.md) 获取进一步的解决方案。

<div style="display:flex; justify-content:center; padding:30px;"><img src="https://s2.loli.net/2023/04/22/nePyWYTZJ4mLwHA.png" style="zoom: 50%;" /></div>

新窗口打开后，点击【打开存档】，在选择文件夹的窗口里找到.minecraft/saves 文件夹，也就是你的存档文件夹，之后选择你所需要转换的存档文件，再点击打开，最终进行转换即可。

之间可能会存在无响应等状况，这是正常现象，且取决于您存档内容的多少。

<div style="display:flex; justify-content:center; padding:30px;"><img src="https://s2.loli.net/2023/04/22/vl8sDKbRPB6fSyh.png" style="zoom: 50%;" /></div>

**仅**出现窗口【Success!】后表示转换成功。若在出现【Success!】窗口之前出现了别的报错窗口，则代表着存档转换可能并不完美，你可以自行进入游戏查看情况，或在B站私信或加QQ群向我们反馈。

<div style="display:flex; justify-content:center; padding:30px;"><img src="https://s2.loli.net/2023/04/22/qUr3lDIR2oQsZWA.png" style="zoom: 50%;" /></div>

若有任何疑惑，欢迎向我们反馈或询问。[反馈链接](https://yuushya.com/townscape/feedback.html)


