import { defineClientConfig } from '@vuepress/client'
import VueForm from '@lljj/vue3-form-element';
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/index.css'
import VueCodemirror from 'vue-codemirror'
import * as Icons from '@element-plus/icons-vue'

export default defineClientConfig({
    enhance({ app, router, siteData }) {
        app.use(ElementPlus)
        // icon
        for (const icon in Icons) {
            // eslint-disable-next-line import/namespace
            app.component('ElIcon' + icon, Icons[icon])
        }
        app.component("VueForm", VueForm)
        app.use(VueCodemirror)
    },
    setup() {},
    rootComponents: [],
})