import java.nio.file.Files
import kotlin.io.path.Path
import org.siouan.frontendgradleplugin.infrastructure.gradle.InstallFrontendTask
import org.siouan.frontendgradleplugin.infrastructure.gradle.RunNpm
import org.siouan.frontendgradleplugin.infrastructure.gradle.RunPnpm

plugins {
    id("org.siouan.frontend-jdk17")
}

repositories {
    mavenCentral()
}

frontend {
    nodeDistributionProvided = true
    nodeInstallDirectory.set(File(providers.environmentVariable("NODEJS_HOME").get()) )
    nodeVersion="18.14.0"
    assembleScript="run docs:build"
    cleanScript="run clean"
    checkScript="run check"
    verboseModeEnabled=true
}

tasks.register<RunPnpm>("dev"){
    group = "Frontend"
    script = "docs:dev"
}

tasks.named<InstallFrontendTask>("installFrontend") {
    val ciPlatformPresent = providers.environmentVariable("CI").isPresent()
    val lockFilePath = "${projectDir}/pnpm-lock.yaml"
    val retainedMetadataFileNames: Set<String>
    if (ciPlatformPresent) {
        retainedMetadataFileNames = setOf(lockFilePath)
    } else {
        // The naive configuration below allows to skip the task if the last successful execution did not change neither
        // the package.json file, nor the pnpm-lock.yaml file, nor the node_modules directory. Any other scenario where
        // for example the lock file is regenerated will lead to another execution before the task is "up-to-date"
        // because the lock file is both an input and an output of the task.
        retainedMetadataFileNames = mutableSetOf("${projectDir}/package.json")
        if (Files.exists(Path(lockFilePath))) {
            retainedMetadataFileNames.add(lockFilePath)
        }
        outputs.file(lockFilePath).withPropertyName("lockFile")
    }
    inputs.files(retainedMetadataFileNames).withPropertyName("metadataFiles")
    outputs.dir("${projectDir}/node_modules").withPropertyName("nodeModulesDirectory")
}
