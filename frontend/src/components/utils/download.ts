export function downloadJson(json,fileName){//fileName要自带.json
    const url = `data:,${JSON.stringify(json,null,2)}`
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
}

export function downloadStream(stream,filename,downloadType) {
    // 创建a标签
    const link = document.createElement("a")
    try {
        link.download = filename
        // 2.使用接口返回的数据下载
        link.href = window.URL.createObjectURL(new Blob([stream], {type: downloadType}))
        // 新标签中下载
        link.target = "_blank"
        link.style.display = "none"
        // 将a标签添加到dom中s
        document.body.appendChild(link)
        // 下载文件
        link.click()
    }
    finally {
        link.href && URL.revokeObjectURL(link.href)
        // 从dom中移除a标签
        document.body.removeChild(link)
    }
}