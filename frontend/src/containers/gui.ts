import { containerPlugin } from '@vuepress/plugin-container'

export const containerGUI = containerPlugin({
    type: "gui",
    before(info: string) {
        return `<json-form schema='`
    },
    after(info: string) {
        return `'/>`
    }
})