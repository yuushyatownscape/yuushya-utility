import { navbar } from "vuepress-theme-hope";

export const zhNavbar = navbar([
    {
        text: "首页",
        icon: "home",
        link: "https://yuushya.com/t"
    },
    {
        text: "指南",
        link: "/guide"
    },
    {
        text: "工具",
        link:"/tool"
    }
]);