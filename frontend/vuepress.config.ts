import { defineUserConfig, viteBundler } from "vuepress";
import { registerComponentsPlugin } from '@vuepress/plugin-register-components';
import { path } from '@vuepress/utils';
import theme from "./src/theme.js";
import { containerGUI } from "./src/containers/gui.js";

export default defineUserConfig({
  bundler: viteBundler({
    viteOptions: {
      optimizeDeps: {
        include: ['lodash'],
      },
    },
  }),
  plugins: [
    containerGUI,
    registerComponentsPlugin({
      componentsDir: path.resolve(__dirname, './src/components'),
      getComponentName(filename) {
        return path.trimExt(filename.replace(/\/|\\/g, '-')).split('-').map(word => word.replace(/^\w/, letter => letter.toUpperCase())).join('')
      },
    })
  ],

  base: "/townscape/docs/",
  public:"./src/public",

  dest: path.resolve(__dirname, '../backend/build/resources/main/static'),

  locales: {
    "/": {
      lang: "zh-CN",
      title: "Yuushya Utils",
      description: "Yuushya Utils",
    },
  },

  theme,

  shouldPrefetch: false,
});
