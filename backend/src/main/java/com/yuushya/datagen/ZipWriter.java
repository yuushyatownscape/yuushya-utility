package com.yuushya.datagen;

import com.yuushya.ui.YuushyaLog;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.ZipOutputStream;

public class ZipWriter {
    private final Path _basePath;
    private final Path _resPath;
    public ZipWriter(Path basePath,Path resPath){
        this._basePath=basePath;
        this._resPath =resPath;
    }
    public void zip(){
        try(ZipOutputStream out = new ZipOutputStream(new FileOutputStream(_resPath.toFile()))){
            JarCreator.zip(out, _basePath.toFile(),"");
            out.flush();
        }
        catch (Exception e){e.printStackTrace(); YuushyaLog.error(e);}
    }

    public void zip(ZipOutputStream out) throws Exception {
        JarCreator.zip(out, _basePath.toFile(),"");
    }

}
