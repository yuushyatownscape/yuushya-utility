package com.yuushya.utils.config


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
class CrosConfig {
    @Bean
    fun corsConfigurer():WebMvcConfigurer {
        return object : WebMvcConfigurer {
            override fun addCorsMappings(registry:CorsRegistry) {
                registry.addMapping("/**")
                    .allowCredentials(false)
                    .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
                    .allowedOrigins("*");
            }
        };
    }

}
