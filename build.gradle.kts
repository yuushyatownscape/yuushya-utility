group = "com.yuushya"
version = "2.1.1"

tasks.register("build-tools-1"){
    dependsOn(":frontend:build")
    finalizedBy(":backend:bootWar")
}
tasks.register("build-tools-2",) {
    file("./build/start.bat").writeText(
        """start http://localhost:9091/
        |java -jar yuushya-utils-${version}.war 
        |@pause""".trimMargin())
//--server.port=${port} should be defined
}
tasks.register<Copy>("build-tools"){
    group = "Build"
    dependsOn("build-tools-1")
    mustRunAfter(":backend:bootWar")
    from("./backend/build/libs/backend-0.0.0.war")
    into("./build/")
    rename { "yuushya-utils-${version}.war" }
    finalizedBy("build-tools-2")
}